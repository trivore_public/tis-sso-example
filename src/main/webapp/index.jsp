<!DOCTYPE html>
<%@page import="com.trivore.oneportal.example.sso.LogoutServlet"%>
<%@page import="com.trivore.oneportal.example.sso.SSOUser"%>
<%@page import="com.trivore.oneportal.example.sso.SSOManager"%>
<%@page import="com.trivore.oneportal.example.sso.Responses"%>
<%@page import="java.net.URI"%>
<%@page import="javax.ws.rs.core.UriBuilder"%>
<%@page import="java.time.Instant"%>
<%@page import="java.util.Enumeration"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String title = "SSO Mgmt API  example";
	SSOUser user = null;
	if (session == null) {
		Responses.send(response, HttpServletResponse.SC_UNAUTHORIZED,
				"You are not signed in: No active session available");
	} else {
		SSOManager sm = SSOManager.getInstance();
		user = sm.getUser(session);
	}
%>
<html>
<head>
<title><%=title%></title>
</head>
<body>
	<h3><%=title%></h3>
	<% if (session == null || user == null) { %>
	<p>You are not signed in</p>
	<% } else { %>
	<p>You are signed in as <%=user%></p>
	<a href="<%=LogoutServlet.createURI(true).toString()%>">Sign-out</a>
	<% } %>
</body>
</html>
