package com.trivore.oneportal.example.sso;

import java.io.Serializable;

import javax.json.JsonObject;

public class SSOUser implements Serializable {
	
	public static final long serialVersionUID = 1;
	
	private String firstName;
	private String lastName;
	private String username;
	private String email;
	private String mobile;
	
	public SSOUser() {
		super();
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getMobile() {
		return mobile;
	}
	
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	private String formatName() {
		if (firstName != null) {
			if (lastName != null) {
				return firstName + " " + lastName;
			} else {
				return firstName;
			}
		} else if (lastName != null) {
			return lastName;
		}
		return null;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName()).append("(");
		sb.append(username);
		String name = formatName();
		if(name != null) {
			sb.append(", name=").append(name);
		}
		if(email != null) {
			sb.append(", email=").append(email);
		}
		if(mobile != null) {
			sb.append(", mobile=").append(mobile);
		}
		sb.append(")");
		return sb.toString();
	}
	
	public static SSOUser createFrom(JsonObject json) {
		SSOUser user = new SSOUser();
		JsonObject name = json.getJsonObject("name");
		user.setFirstName(name.getString("givenName", null));
		user.setLastName(name.getString("familyName", null));
		user.setUsername(json.getString("username"));
		user.setEmail(json.getString("email", null));
		user.setMobile(json.getString("mobile", null));
		return user;
	}
}
