package com.trivore.oneportal.example.sso;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

public class Responses {
	
	private Responses() {
		/* Static methods only */
	}
	
	public static void send(HttpServletResponse response, int statusCode, String message) {
		try (PrintWriter pw = response.getWriter()) {
			response.setStatus(statusCode);
			pw.println(message);
		} catch (IOException e) {
			System.out.println("Failed to write response");
			e.printStackTrace();
		}
	}
	
	public static void missingParameter(HttpServletResponse response, String paramName) {
		badRequest(response, "Missing mandatory parameter: " + paramName);
	}
	
	public static void invalidParameter(HttpServletResponse response, String paramName, String paramValue) {
		badRequest(response, "Invalid parameter: " + paramName + "=" + paramValue);
	}
	
	public static void badRequest(HttpServletResponse response, String message) {
		Responses.send(response, HttpServletResponse.SC_BAD_REQUEST, message);
	}
	
	/**
	 * Send 16x16 px icon as response. This icon may be shown
	 * on user's browser when signing in to this service.
	 * 
	 * @param response Http servlet response
	 */
	public static void sendIcon(HttpServletResponse response) {
		byte[] icon = Resources.getIcon();
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentType("image/png");
		response.setContentLength(icon.length);
		try (OutputStream out = response.getOutputStream()) {
			out.write(icon);
		} catch (Exception e) {
			System.out.println("Failed to write icon response");
			e.printStackTrace();
		}
	}
}
