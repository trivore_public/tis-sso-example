package com.trivore.oneportal.example.sso;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractExampleServlet extends HttpServlet {
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		try {
			process(request, response);
		} catch (Exception e) {
			System.out.println("SSO request processing failed");
			e.printStackTrace();
			Responses.send(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Failed to process SSO request");
		}
	}
	
	protected abstract void process(HttpServletRequest request, HttpServletResponse response);
}
