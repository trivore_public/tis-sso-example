package com.trivore.oneportal.example.sso;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Example servlet that implements onePortal Single sign-on using Management API
 */
@WebServlet(SsoCallbackServlet.PATH)
public class SsoCallbackServlet extends AbstractExampleServlet {
	
	public static final String PATH = "/sso-callback";
	
	@Override
	protected void process(HttpServletRequest request, HttpServletResponse response) {
		SSOToken token = SSOToken.createFrom(request);
		System.out.printf("Received Mgmt API SSO request: token=%s, validity=%s\n", token.getToken(),
				token.getValidTo());
		
		/* Create session for request from browser (JSESSIONID cookie will be sent as response). */
		HttpSession session = request.getSession(true);
		
		/* Store SSO request information to session for possible later retrieval.
		 * It is recommended to return response as fast possible from this servlet
		 * to make sure the browser receives cookie for later identification.
		 * 
		 * Fetching user information from REST endpoint can be deferred until the
		 * user actually enters the external service.
		 * */
		saveForLater(session, token);
		
		Responses.sendIcon(response);
	}
	
	/**
	 * Save SSO parameters for later use. This method simply stores these parameters to http session
	 * so they can be used to retrieve user information at a later time. Any possible previously
	 * signed-in user is removed from http session unless the token is equal to the previously
	 * used token in which case no action is taken (this is important because token can only be used
	 * once).
	 * 
	 * @param session Currently active http session
	 * @param ssoToken SSO token received from browser
	 * @return true if parameters were successfully stored to session, false otherwise.
	 */
	public void saveForLater(HttpSession session, SSOToken ssoToken) {
		SSOToken prevToken = SSOSession.getSsoToken(session);
		if (prevToken != null && prevToken.getToken().equals(ssoToken.getToken())) {
			/* Received same token again, do nothing. */
			System.out.println("SSO callback ignored: same SSO token received again: " + ssoToken.getToken());
			return;
		}
		
		/* Setup new token. */
		SSOSession.setSsoToken(session, ssoToken);
		
		/* Remove previously signed in user from session. */
		SSOSession.removeUser(session);
	}
}
