package com.trivore.oneportal.example.sso;

import java.net.URI;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(LogoutServlet.PATH)
public class LogoutServlet extends AbstractExampleServlet {
	
	public static final String PATH = "/logout";
	
	@Override
	protected void process(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("Received Mgmt API logout request");
		boolean redirect = Boolean.parseBoolean(request.getParameter("redirect"));
		HttpSession session = request.getSession(false);
		if (session != null) {
			SSOSession.clearSession(session);
		} else {
			System.out.println("No active session available");
		}
		if (redirect) {
			/* Redirect user back to /index.jsp if asked to do so.
			 * This is not required when request is performed as part of SSO logout,
			 * this is used only when user manually clicks logout button. */
			try {
				response.sendRedirect(SSOManager.getContextPath());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static URI createURI(boolean redirect) {
		return SSOManager.buildURI(PATH).queryParam("redirect", redirect).build();
	}
}
