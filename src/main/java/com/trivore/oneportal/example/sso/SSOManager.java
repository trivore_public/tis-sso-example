package com.trivore.oneportal.example.sso;

import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.time.Instant;
import java.util.Properties;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

/**
 * This class handle the actual Single sign-on procedure and parameter
 * storage to http session.
 */
@WebListener
public class SSOManager implements ServletContextListener {
	
	private static final String PROP_CONFIG_LOCATION = "sso.example.configLocation";
	private static final String PROP_API_URL = "sso.example.api.url";
	private static final String PROP_API_CLIENT_ID = "sso.example.api.clientId";
	private static final String PROP_API_CLIENT_SECRET = "sso.example.api.clientSecret";
	
	private static ServletContext ctx;
	
	private static SSOManager instance;
	
	private Client apiClient;
	
	private String apiUrl;
	
	public SSOManager() {
		super();
	}
	
	/**
	 * Called by servlet container on webapp initialisation.
	 * 
	 * @param sce Servlet context event
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ctx = sce.getServletContext();
		
		/* Setup REST API client with basic authentication */
		
		Properties config = new Properties();
		
		String configLocation = System.getenv("SSO_EXAMPLE_CONFIG_LOCATION");
		if (configLocation == null || configLocation.isEmpty()) {
			configLocation = getConfigProp(config, ctx, PROP_CONFIG_LOCATION);
		}
		
		if (configLocation != null && !configLocation.isEmpty()) {
			System.out.println("Loading configuration from " + configLocation);
			URL configURL;
			try {
				configURL = new URL(configLocation);
			} catch (Exception e) {
				IllegalArgumentException iae = new IllegalArgumentException(
						String.format("Invalid URL: %s=%s", PROP_CONFIG_LOCATION, configLocation), e);
				if (!configLocation.startsWith("file:")) {
					try {
						configURL = new URL("file:" + configLocation);
					} catch (Exception e2) {
						throw iae;
					}
				} else {
					throw iae;
				}
			}
			
			try (InputStream input = configURL.openStream()) {
				config.load(input);
			} catch (Exception e) {
				throw new IllegalArgumentException(
						String.format("Failed to load URL: %s=%s", PROP_CONFIG_LOCATION, configLocation), e);
			}
		}
		
		apiUrl = getConfigProp(config, ctx, PROP_API_URL);
		String apiClientId = getConfigProp(config, ctx, PROP_API_CLIENT_ID);
		String apiClientSecret = getConfigProp(config, ctx, PROP_API_CLIENT_SECRET);
		
		System.out.println("Using API URL:           " + apiUrl);
		System.out.println("Using API Client ID:     " + apiClientId);
		System.out.println("Using API Client secret: " + apiClientSecret);
		
		apiClient = ClientBuilder.newClient();
		apiClient.register(new BasicAuthenticator(apiClientId, apiClientSecret));
		
		System.out.printf("Initialized SSO manager with clientId=%s\n", apiClientId);
		
		instance = this;
	}
	
	private String getConfigProp(Properties config, ServletContext ctx, String name) {
		String value = System.getProperty(name);
		if (value == null || value.isEmpty()) {
			value = config.getProperty(name);
		}
		if (value == null || value.isEmpty()) {
			value = ctx.getInitParameter(name);
		}
		if (value == null || value.isEmpty()) {
			throw new IllegalArgumentException("Missing required config property: " + name);
		}
		return value;
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}
	
	public static SSOManager getInstance() {
		return instance;
	}
	
	/**
	 * Get currently signed-in user. Returns user stored to http session or
	 * retrieves user information using SSO parameters stored to session. If
	 * no user or SSO parameters are available, returns null.
	 * 
	 * @param session currently active http session.
	 * @return Signed-in user or null if not available.
	 */
	public SSOUser getUser(HttpSession session) {
		SSOUser user = SSOSession.getUser(session);
		if (user != null) {
			return user;
		}
		
		SSOToken ssoToken = SSOSession.getSsoToken(session);
		
		if (ssoToken == null || ssoToken.getToken() == null || ssoToken.getValidTo() == null) {
			System.out.println("Session does not contain SSO information");
			return null;
		}
		
		Instant validTo = ssoToken.getValidTo();
		try {
			if (validTo.isBefore(Instant.now())) {
				System.out.println("SSO token has expired on " + validTo);
				
				/* Remove expired token */
				SSOSession.removeSsoToken(session);
				return null;
			}
			
			String token = ssoToken.getToken();
			WebTarget ssoTarget = apiClient.target(apiUrl).path("sso").path("token").path(token);
			Response ssoResponse = ssoTarget.request().post(null);
			int status = ssoResponse.getStatus();
			if (status == HttpServletResponse.SC_NOT_FOUND) {
				System.out.println("SSO user information not found for token=" + ssoToken);
				return null;
			} else if (status != HttpServletResponse.SC_OK) {
				System.out.println("Unexpected status code: " + status);
				return null;
			}
			String jsonResponse = ssoResponse.readEntity(String.class);
			System.out.println("Single sign-on response:");
			System.out.println(jsonResponse);
			JsonReader reader = Json.createReader(new StringReader(jsonResponse));
			JsonObject jsonUser = reader.readObject();
			user = SSOUser.createFrom(jsonUser);
			System.out.println("Successful Single sign-on: " + user.getUsername());
			SSOSession.setUser(session, user);
			session.setMaxInactiveInterval(0);
			return user;
		} catch (Exception e) {
			System.out.println("Failed to process SSO request");
			e.printStackTrace();
			return null;
		}
	}
	
	public static UriBuilder buildURI(String path) {
		return UriBuilder.fromPath(getContextPath()).path(path);
	}
	
	public static String getContextPath() {
		return ctx.getContextPath();
	}
}
