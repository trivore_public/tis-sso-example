package com.trivore.oneportal.example.sso;

import javax.servlet.http.HttpSession;

public class SSOSession {
	
	/* Single sign-on session attribute names */
	public static final String ATTR_USER = "sso-user";
	public static final String ATTR_SSO_TOKEN = "sso-token";
	
	public static SSOUser getUser(HttpSession session) {
		if (session == null) {
			return null;
		}
		Object user = session.getAttribute(ATTR_USER);
		if (user == null) {
			return null;
		} else if (user instanceof SSOUser) {
			return (SSOUser) user;
		} else {
			throw new IllegalArgumentException("Invalid SSO user object: " + user);
		}
	}
	
	public static void setUser(HttpSession session, SSOUser user) {
		session.setAttribute(ATTR_USER, user);
	}
	
	public static void removeUser(HttpSession session) {
		session.removeAttribute(ATTR_USER);
	}
	
	public static SSOToken getSsoToken(HttpSession session) {
		return (SSOToken) session.getAttribute(ATTR_SSO_TOKEN);
	}
	
	public static void setSsoToken(HttpSession session, SSOToken ssoToken) {
		session.setAttribute(ATTR_SSO_TOKEN, ssoToken);
	}
	
	public static void removeSsoToken(HttpSession session) {
		session.removeAttribute(ATTR_SSO_TOKEN);
	}
	
	public static void clearSession(HttpSession session) {
		
		SSOSession.removeUser(session);
		SSOSession.removeSsoToken(session);
	}
}
